# Credit: https://gist.github.com/svrist/73e2d6175104f7ab4d201280acba049c
# Modified to load parameters in yml and pass CAPABILITY_NAMED_IAM to the calls

# Example syntax:
#   python create_update_cf_stack.py [stack_name] [template_file] [parameter_file]

# Example parameter file:
#  - ParameterKey: AppName
#    ParameterValue: KeyPool
#    UsePreviousValue: False
#  - ParameterKey: DomainName
#    ParameterValue: andrewskeypool.com
#    UsePreviousValue: False

'Update or create a stack given a name and template + params'
from __future__ import division, print_function, unicode_literals

from datetime import datetime
import logging
import json
import yaml
import re
import sys

import boto3
import botocore

cf = boto3.client('cloudformation')  # pylint: disable=C0103
iam = boto3.client('iam')  # pylint: disable=C0103
log = logging.getLogger('deploy.cf.create_or_update')  # pylint: disable=C0103

policy = {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ListOfPolicies",
            "Effect": "Allow",
            "Action": [],
            "Resource": "*"
        }
    ]
}


def main(stack_name, template, parameters):
    'Update or create stack'

    template_data = _parse_template(template)
    parameter_data = _parse_parameters(parameters)
    role = _create_role()

    params = {
        'StackName': stack_name,
        'TemplateBody': template_data,
        'Parameters': parameter_data,
        'Capabilities': ['CAPABILITY_NAMED_IAM'],
        'RoleARN': role,
    }

    attempts = 0
    while True:
        # Delete can fail because of lacking permissions so we run it until it succeeds
        deleteSuccess = False
        while not deleteSuccess:
            deleteSuccess = _delete_stack_if_exists(stack_name) # Returns true on successful delete
        
        deploySuccess = _attempt_deploying_stack(stack_name, params) # Returns true on successful create
        if deploySuccess:
            print("Successfully deployed the stack, performing final delete of stack...")
            # Final delete to make sure there aren't any missing delete perms and cleanup
            deleteSuccess = True
            while not deleteSuccess:
                deleteSuccess = _delete_stack_if_exists(stack_name)

            _delete_role() # Cleanup temp role

            # Print final list of needed perms
            print("Here was the successful policy list")
            print(policy)
            return


def _delete_role():
    try:
        print("Deleting temporary role...")
        response = iam.get_role(
            RoleName='permission_finder_temp_role'
        )
    except botocore.exceptions.ClientError as ex:
        raise
    else:
        print("Deleted role sucessfully")
        return
    
def _attempt_deploying_stack(stack_name, params):
    try:
        print('Creating stack...')
        stack_result = cf.create_stack(**params)
        waiter = cf.get_waiter('stack_create_complete')
        print("...waiting for stack to be ready...")
        waiter.wait(StackName=stack_name)
    except botocore.exceptions.ClientError as ex:
        print("Encountered error deploying stack")
        print(ex.response)
        raise
    except botocore.exceptions.WaiterError as ex:
        if 'ROLLBACK_COMPLETE' in str(ex) or 'ROLLBACK_FAILED' in str(ex):
            events = cf.describe_stack_events(StackName=stack_name)
            # print(events)
            for e in events['StackEvents']:
                if e['ResourceStatus'] == 'CREATE_FAILED':
                    # print("\n" + e['ResourceStatusReason'])
                    regexMatches = re.search(r'AccessDenied. User doesn\'t have permission to call (\S+)', e['ResourceStatusReason'])
                    if regexMatches:
                        neededPerm = regexMatches.group(1)
                        print("This permission is needed: {}".format(neededPerm))
                        policy['Statement'][0]['Action'].append(neededPerm)
                        continue

                    regexMatches = re.search(r'is not authorized to perform: (\S+)', e['ResourceStatusReason'])
                    if regexMatches:
                        neededPerm = regexMatches.group(1)
                        print("This permission is needed: {}".format(neededPerm))
                        policy['Statement'][0]['Action'].append(neededPerm)
                        continue

                    print("Couldn't find a permission in the resource status:")
                    print(e['ResourceStatusReason'])
                    
            _set_policy()
            return False
            
        else:
            print("Encountered error deploying stack")
            print(ex)
            raise
    else:
        print("Stack was successfully created")
        print(json.dumps(
            cf.describe_stacks(StackName=stack_result['StackId']),
            indent=2,
            default=json_serial
        ))
        return True


def _delete_stack_if_exists(stack_name):
    try:
        print("Deleting stack...")
        response = cf.delete_stack(
            StackName=stack_name
        )
        waiter = cf.get_waiter('stack_delete_complete')
        print("...waiting for stack to be deleted...")
        waiter.wait(StackName=stack_name)
    except botocore.exceptions.ClientError as ex:
        error_message = ex.response['Error']['Message']
        print("Error encountered while deleting stack!")
        print(error_message)
        raise
    except botocore.exceptions.WaiterError as ex:
        if 'DELETE_FAILED' in str(ex):
            events = cf.describe_stack_events(StackName=stack_name)
            # print(events)
            for e in events['StackEvents']:
                if e['ResourceStatus'] == 'DELETE_FAILED':
                    # print("\n" + e['ResourceStatusReason'])
                    if (e['ResourceStatusReason'] == "Resource creation cancelled"):
                        continue

                    regexMatches = re.search(r'is not authorized to perform: (\S+)', e['ResourceStatusReason'])
                    if not regexMatches:
                        print("Couldn't find a permission in the resource status")
                        print("\t" + e['ResourceStatusReason'])
                        continue

                    neededPerm = regexMatches.group(1)
                    print("This permission is needed for delete: {}".format(neededPerm))
                    policy['Statement'][0]['Action'].append(neededPerm)
                    
            _set_policy()
            return False
        else:
            print("Encountered error deploying stack")
            print(ex)
            raise
    else:
        print('Deleted stack successfully')
        return True


def _set_policy():
    try:
        print("Setting policy actions to:")
        print(policy['Statement'][0]['Action'])
        iam.put_role_policy(
            RoleName='permission_finder_temp_role',
            PolicyName='permission_finder_temp_policy',
            PolicyDocument=json.dumps(policy)
        )
    except botocore.exceptions.ClientError as ex:
        error_message = ex.response['Error']['Message']
        print("Error encountered while creating updating policy!")
        print(error_message)
        raise
    else:
        print('Updated policy')

def _create_role():
    try:
        response = iam.get_role(
            RoleName='permission_finder_temp_role'
        )
    except botocore.exceptions.ClientError as ex:
        pass
    else:
        print('Found role {}'.format(response['Role']['Arn']))
        try:
            print('Clearing out any existing policies...')
            iam.delete_role_policy(
                RoleName='permission_finder_temp_role',
                PolicyName='permission_finder_temp_policy'
            )
        except:
            print("Encountered error deleting policy, going to try and continue")
            print(sys.exc_info()[0])
            pass
        
        return response['Role']['Arn']

    try:
        assume_role_policy_document = json.dumps({
            "Version": "2012-10-17",
            "Statement": [
                {
                "Effect": "Allow",
                "Principal": {
                    "Service": "cloudformation.amazonaws.com"
                },
                "Action": "sts:AssumeRole"
                }
            ]
        })
        response = iam.create_role(
            RoleName='permission_finder_temp_role',
            AssumeRolePolicyDocument=assume_role_policy_document,
            Description='Temporary role used for testing which permissions a cft requires'
        )
    except botocore.exceptions.ClientError as ex:
        error_message = ex.response['Error']['Message']
        print("Error encountered while creating IAM role!")
        print(error_message)
        raise
    else:
        print('Created role {}'.format(response['Role']['Arn']))
        return response['Role']['Arn']


def _parse_template(template):
    with open(template) as template_fileobj:
        template_data = template_fileobj.read()
    cf.validate_template(TemplateBody=template_data)
    return template_data


def _parse_parameters(parameters):
    with open(parameters) as parameter_fileobj:
        parameter_data = yaml.safe_load(parameter_fileobj)
    return parameter_data


def _stack_exists(stack_name):
    stacks = cf.list_stacks()['StackSummaries']
    for stack in stacks:
        if stack['StackStatus'] == 'DELETE_COMPLETE':
            continue
        if stack_name == stack['StackName']:
            return True
    return False


def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""
    if isinstance(obj, datetime):
        serial = obj.isoformat()
        return serial
    raise TypeError("Type not serializable")


if __name__ == '__main__':
    main(*sys.argv[1:])