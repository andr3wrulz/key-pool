# Key Pool Account Setup

1. Register a domain name in Route53
1. Deploy the [account-setup.yml](account-setup.yml) CloudFormation template
    - If the CertificateManagerCertificate resource is stuck at `CREATE_IN_PROGRESS`, you may have to go the Certificate Manager console and perform the DNS validation (button press to perform the DNS challenge in Route53)
    - The CloudFront distribution can take up to 25 minutes to finish (static website content won't be available until this is done)
    - The UserPool Domain (login/signup pages) takes about 15 minutes to be created and change to `Active` (Cognito > User Pools > [user pool] > App Integration > Domain Name)
1. Generate programatic access keys for the user created by the template and put them in the gitlab variables