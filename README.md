# Key Pool

AWS serverless application for sharing keys

## References
- [Creating a Static Website using CloudFormation](https://nickolaskraus.org/articles/creating-a-static-website-using-cloudformation/)
- [CFT for s3 cloudfront certificate](https://gist.github.com/jonoirwinrsa/ceb2cba3d104720320f30e06b8c821f8)
- [AWS Amplify and Vue Getting Started](https://docs.amplify.aws/start/q/integration/vue)
- [Create a Cognito Authentication Backend via CloudFormation](https://gist.github.com/singledigit/2c4d7232fa96d9e98a3de89cf6ebe7a5)
- [How to Build Production-ready Vue Authentication](https://dev.to/dabit3/how-to-build-production-ready-vue-authentication-23mk)
- [Deploy an AWS AppSync GraphQL API with CloudFormation](https://adrianhall.github.io/cloud/2018/04/17/deploy-an-aws-appsync-graphql-api-with-cloudformation/)
- [AWS Tutorial: DynamoDB Resolvers](https://docs.aws.amazon.com/appsync/latest/devguide/tutorial-dynamodb-resolvers.html)
- [How to setup federated identity providers for social](https://docs.aws.amazon.com/cognito/latest/developerguide/cognito-user-pools-configuring-federation-with-social-idp.html)
- [Python script for creating/updating cloud formation stacks](https://gist.github.com/svrist/73e2d6175104f7ab4d201280acba049c)