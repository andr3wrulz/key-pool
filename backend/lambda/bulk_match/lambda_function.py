import json
import boto3
import requests
from boto3.dynamodb.conditions import Key,Attr
from os import environ

# Pass api key, no fuzzy search, only one result, no dlc, start of search
SEARCH_URL_BASE = 'https://api.rawg.io/api/games' \
    '?key={api_key}' \
    '&search_precise=true' \
    '&page_size=1' \
    '&exclude_additons=true' \
    '&search='.format(api_key=environ.get('RAWG_API_KEY'))

def get_all_unmatched(key_table):
    print('Looking for keys with no match id...')
    response = key_table.scan(
        FilterExpression=Attr('rawgId').not_exists()
    )
    print('Found {} unmatched keys'.format(len(response['Items'])))
    return response['Items']

def get_all_keys(key_table):
    print('Getting ALL keys regardless of existing match...')
    response = key_table.scan()
    print('Found {} keys'.format(len(response['Items'])))
    return response['Items']
    
def look_for_matches(key_list):
    print('Looking for matches...')
    matches = []
    for key in key_list:
        print('\tLooking for match for \'{}\'...'.format(key['name']))
        url = SEARCH_URL_BASE + requests.utils.quote(key['name'])
        response = requests.get(url).json()
        if response.get('results', [])[0]:
            print('\tFound: \'{}\' id \'{}\''.format(response['results'][0]['name'], response['results'][0]['id']))
            matches.append({
                'KeyId': key['KeyId'],
                'rawgId': response['results'][0]['id'],
                'genres': ', '.join(map(lambda x: x.get('name', ''), response['results'][0].get('genres', []))),
                'metacritic': response['results'][0].get('metacritic', 0),
                'released': response['results'][0].get('released', '')
            })
    print('Out of {} keys, we found matches for {}'.format(len(key_list), len(matches)))
    return matches
    
def update_matches(to_update, key_table):
    print('Updating matches in the database...')
    for key in to_update:
        response = key_table.update_item(
            Key={
                'KeyId': key['KeyId']
            },
            UpdateExpression="set rawgId=:rawgId, genres=:genres, metacritic=:metacritic, released=:released",
            ExpressionAttributeValues={
                ':rawgId': key['rawgId'],
                ':genres': key['genres'],
                ':metacritic': key['metacritic'],
                ':released': key['released']
            },
            ReturnValues="UPDATED_NEW"
        )
    print('Finished updating keys')

def lambda_handler(event, context):
    # Get a dynamo object for the table 
    dynamodb = boto3.resource('dynamodb')
    key_table = dynamodb.Table(environ.get('TABLE_NAME', 'KeyPool-app-keys-table'))

    print('Received event: {}'.format(json.dumps(event)))

    if event.get('force_update_all', '') == True:
        unmatched_games = get_all_keys(key_table)
    else:
        unmatched_games = get_all_unmatched(key_table)
    
    matches_to_update = look_for_matches(unmatched_games)
    
    update_matches(matches_to_update, key_table)
    
    return {
        'statusCode': 200,
        'body': json.dumps('Out of {} keys without matches, we found matches for {}!'.format(len(unmatched_games), len(unmatched_games)))
    }
