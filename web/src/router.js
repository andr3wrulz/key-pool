import VueRouter from 'vue-router'
import { Auth } from 'aws-amplify'

import Home from './components/Home'
// import Profile from './components/Profile'
import AuthComponent from './components/Auth'
import Submit from './components/Submit'
import Game from './components/Game'
import EditGame from './components/EditGame'
import Redeem from './components/Redeem'
import Match from './components/Match'
import BulkImport from './components/BulkImport'
import AdminPanel from './components/AdminPanel'

const routes = [
  { path: '/', component: Home },
  { path: '/auth', component: AuthComponent },
  { path: '/submit', component: Submit, meta: {requiresAuth: true} },
//  { path: '/profile', component: Profile, meta: {requiresAuth: true} },
  { path: '/game/:gameId', component: Game, name: 'game' },
  { path: '/editgame/:gameId', component: EditGame, name: 'editGame', meta: {requiresAuth: true} },
  { path: '/redeem/:gameId', component: Redeem, name: 'redeem', meta: {requiresAuth: true} },
  { path: '/match/:gameId', component: Match, name: 'match', meta: {requiresAuth: true} },
  { path: '/admin', component: AdminPanel, name: 'admin', meta: {requiresAuth: true}, props: true },
  { path: '/bulkimport', component: BulkImport, name: 'bulkImport', meta: {requiresAuth: true}, props: true }
]

const router = new VueRouter({
  routes
})

router.beforeResolve((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    Auth.currentAuthenticatedUser().then(() => {
      next()
    }).catch(() => {
      next({
        path: '/auth',
        query: {
          redirect: to.fullPath,
        }
      });
    });
  }
  next()
})

// Workaround for debouncing duplicated push errors
const originalPush = VueRouter.prototype.push;
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => {
    if (err.name !== 'NavigationDuplicated') throw err
  });
}

export default router