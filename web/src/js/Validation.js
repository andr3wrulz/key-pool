export class Validation {

    static validLength(str, min, max) {
        return str.length >= min && str.length <= max
    }

    static validEmail(str) {
        return /^.+@.+\..+$/.test(str)
    }

    static hasNumber(str) {
        return /[0-9]/.test(str)
    }

    static hasUpperAndLowerCase(str) {
        return /[a-z]/.test(str) && /[A-Z]/.test(str)
    }
    
    static hasSpecialCharacter(str) {
        return /[~`!#$%^&*+=\-[\]\\';,/{}|\\":<>?]/.test(str)
    }

    static validPassword(str, min, max) {
        return this.validLength(str, min, max)
            && this.hasNumber(str)
            && this.hasUpperAndLowerCase(str)
            && this.hasSpecialCharacter(str)
    }
}