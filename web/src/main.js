import Vue from 'vue'
import VueRouter from 'vue-router'

// Amplify
import Amplify, { Auth } from 'aws-amplify'
import AWSAppSyncClient from "aws-appsync"
import '@aws-amplify/ui-vue'

// Apollo (http requests)
import VueApollo from "vue-apollo";

// FontAwesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faUser, faLock, faPortrait, faEnvelope, faKey, faGamepad, faEdit,
  faExclamationCircle, faCheckCircle, faClipboard, faExternalLinkAlt
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
  faUser, faLock, faPortrait, faEnvelope, faKey, faGamepad, faEdit,
  faExclamationCircle, faCheckCircle, faClipboard, faExternalLinkAlt
)

Vue.component('font-awesome-icon', FontAwesomeIcon)

import App from './App'
import router from './router'

const config = {
  url: 'https://q26qun7unfaa7jwlmibfachggm.appsync-api.us-east-1.amazonaws.com/graphql',
  region: 'us-east-1',
  auth: {
    type: 'AWS_IAM',
    credentials: () => Auth.currentCredentials()
  }
};
const options = {
  defaultOptions: {
    watchQuery: {
      fetchPolicy: "cache-and-network"
    }
  }
};

const defaultClient = new AWSAppSyncClient(config, options);

const apolloProvider = new VueApollo({ defaultClient });

const awsconfig = {
  Auth: {

    // REQUIRED only for Federated Authentication - Amazon Cognito Identity Pool ID
    identityPoolId: 'us-east-1:8da6000e-bfe2-4805-a922-d4992ff4a5f5',

    // REQUIRED - Amazon Cognito Region
    region: 'us-east-1',

    // OPTIONAL - Amazon Cognito Federated Identity Pool Region 
    // Required only if it's different from Amazon Cognito Region
    //identityPoolRegion: 'XX-XXXX-X',

    // OPTIONAL - Amazon Cognito User Pool ID
    userPoolId: 'us-east-1_TrIHpGDXL',

    // OPTIONAL - Amazon Cognito Web Client ID (26-char alphanumeric string)
    userPoolWebClientId: '2pcc80bmaur982kgemdgfqmf9n',

    // OPTIONAL - Enforce user authentication prior to accessing AWS resources or not
    //mandatorySignIn: false,

    // OPTIONAL - customized storage object
    //storage: MyStorage,

    // OPTIONAL - Manually set the authentication flow type. Default is 'USER_SRP_AUTH'
    //authenticationFlowType: 'USER_PASSWORD_AUTH',

    // OPTIONAL - Manually set key value pairs that can be passed to Cognito Lambda Triggers
    //clientMetadata: { myCustomKey: 'myCustomValue' },

     // OPTIONAL - Hosted UI configuration
    oauth: {
        domain: 'auth.andrewskeypool.com',
        scope: ['phone', 'email', 'profile', 'openid', 'aws.cognito.signin.user.admin'],
        redirectSignIn: 'http://localhost:8080/',
        redirectSignOut: 'http://localhost:8080/',
        responseType: 'code' // or 'token', note that REFRESH token will only be generated when the responseType is code
    }
  }
};

Amplify.configure(awsconfig);

Vue.use(VueRouter)
Vue.use(VueApollo)
Vue.use(require('vue-moment'));
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  apolloProvider,
  router
}).$mount('#app')